FROM ocamlpro/ocaml:4.14-2025-02-02

ARG CATALA_SRC=git+https://github.com/catalaLang/catala

# Install LaTeX requirements
RUN sudo apk add texlive-xetex texmf-dist-latexextra texmf-dist-binextra texmf-dist-pictures texmf-dist-fontsrecommended font-dejavu groff texmf-dist-lang
# Fewer texmf deps should be required once
# https://gitlab.alpinelinux.org/alpine/aports/-/issues/16190 is fixed
# (see note in main catala ci/harness.yml)

# Install catala from source
RUN export OPAMVAR_cataladevmode=1 OPAMVAR_catalaz3mode=1 && \
    opam --cli=2.1 switch create catala ocaml-system && \
    opam --cli=2.1 pin add catala^$CATALA_SRC -n && \
    opam --cli=2.1 install catala && \
    opam --cli=2.1 clean --logs -cr

# Install the Marianne font
RUN sudo apk add fontconfig
RUN curl -L https://www.systeme-de-design.gouv.fr/uploads/Marianne_fd0ba9c190.zip -o /tmp/marianne.zip && \
    unzip /tmp/marianne.zip -d /tmp/marianne && \
    mv "/tmp/marianne/Marianne/fontes desktop/" ~/.fonts && \
    rm -rf /tmp/marianne && \
    fc-cache

# Install catleg
ENV PATH /home/ocaml/.local/bin:$PATH
RUN pip install catleg --pre --break-system-packages

ENTRYPOINT ["opam","exec","--"]
CMD ["sh"]
